import pandas as pd
import numpy as np
import sys
import math
from collections import Counter
from sklearn import tree
from pprint import pprint

#Implements the main DTL algorithm. The class should somehow be encoded in the datapoints structure. A reasonable approach would be to use the feature name "class" to refer to it consistently.
def DTL(datapoints, features, default_value):
    if datapoints.empty:
        return default_value
    elif len(datapoints['class'].unique()) == 1:
        return datapoints.iloc[0]['class']
    elif len(features) == 0:
        return default_value
    else:
        best_feature = choose_attribute(features, datapoints)
        tree = {best_feature:{}}
        best_attrs = datapoints[best_feature].unique()
        features.remove(best_feature)
        for attr in best_attrs:
            new_datapoints = datapoints.loc[datapoints[best_feature] == attr]
            subtree = DTL(new_datapoints, features, mode(datapoints))
            tree[best_feature][attr] = subtree
        return tree

#Returns the feature with the greatest information gain for the provided datapoints.
def choose_attribute(features, datapoints):
    info_gains = [compute_info(datapoints, feature) for feature in features]
    best_attr_index = np.argmax(info_gains)
    best_attr = features[best_attr_index]
    return best_attr

#Computes the mode of the class of datapoints.
def mode(datapoints):
    return datapoints.loc[:,'class'].mode()[0]

#Computes the entropy given an array of probabilities
def entropy(probs):
    return sum([-prob*math.log(prob, 2) for prob in probs])

#Computes the entropy of a column
def entropy_of_column(column):
    count = Counter(attr for attr in column)
    num_instances = len(column)*1.0
    probs = [attr / num_instances for attr in count.values()]
    return entropy(probs)

#Computes the information entropy of datapoints w.r.t the class.
def compute_info(datapoints, column):
    df_split = datapoints.groupby(column)
    
    length = len(datapoints.index) * 1.0
    df_agg_ent = df_split.agg({'class' : [entropy_of_column, lambda x: len(x)/length] })['class']
    df_agg_ent.columns = ['Entropy', 'PropObservations']
    
    new_entropy = sum(df_agg_ent['Entropy'] * df_agg_ent['PropObservations'])
    old_entropy = entropy_of_column(datapoints['class'])
    return old_entropy-new_entropy

def classify(tree, datapoint, classifiers):
    root = list(tree.keys())[0]
    if tree[root][datapoint.iloc[0][root]] in classifiers:
        classification = tree[root][datapoint.iloc[0][root]]
        return classification
    else:
        return classify(tree[root][datapoint.iloc[0][root]], datapoint, classifiers)

def accuracy(prediction, acutal):
    count = 0
    for i in range(len(prediction)):
        if prediction[i] == acutal[i]:
            count += 1
    return count/len(prediction) * 100

def main():
    train = pd.read_csv(sys.argv[1])
    test = pd.read_csv(sys.argv[2])
    
    test_vals = list(test['class'])
    classifiers = train['class'].unique()
    test = test.drop(['class'], axis=1)
    features = list(train.columns)
    features.remove('class')
    default_value = mode(train)
    
    tree = DTL(train, features, default_value)
    pprint(tree)
    
    prediction = []
    for index, row in test.iterrows():
        prediction.append(classify(tree, test.iloc[[index]], classifiers))

    print("Accuracy is", accuracy(prediction, test_vals), "%")

if __name__ == "__main__":
    main()
