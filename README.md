# Assignment 3: Implementation of Decision Tree Learning Algorithm

For this assignment, we implemented DTL. We explored its performance in classifying edible and posionous mushrooms. The performance was then compared to the performance of the CART algorithm from R's 'rpart' package. In 'report.pdf', we discuss the results that we obtained and visualize the trees from both algorithms. 

How to Run the Program
----------------------
- Open the terminal.
- Go the the directory that holds the  source files. 
- To run analysis on the DTL algorithm:
        > python3 DTL.py ../data/full_training.csv ../data/full_testing.csv
